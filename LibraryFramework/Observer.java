package LibraryFramework;

public interface Observer {
    void update();
}
