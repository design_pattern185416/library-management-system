package LibraryFramework;

import java.util.List;

public interface SearchStrategy {
    List<Book> search(String query);
}
