package LibraryFramework;

public interface LibraryOperation {

    void validate();

    void process();

    void update();
}
