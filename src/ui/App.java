package com;

import com.library.observer.LibrarySystem;
import com.library.ui.LoginPage;

public class App {
    public static void main(String[] args) {
        LibrarySystem librarySystem = new LibrarySystem();
        LoginPage loginPage = new LoginPage(librarySystem);
        loginPage.setVisible(true);
    }
}
