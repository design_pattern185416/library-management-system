# Library Management System

# Roles and Responsibility of Each Member

1. Drukden Wangchuk ---- Developer
2. Pema Yangzom -------Developer
3. Jampel Dorji ----------Developer
4. Chencho Dema --------Developer

## Features

1. Login and Logout for Admin
2. New Student Entry
3. New Book Entry
4. Issue Book
5. Return Book
6. Book Records

## Design Patterns Used

1. Singleton: For managing database connections.
2. Factory Method: For creating forms.
3. Observer: For updating UI views based on changes.
4. Strategy: For searching purposes
5. Template: This is mainly for the representing skeleton

## How to Run

1. Run the `App` class to start the application.

## Dependencies

- Java SE 8 or later
- Swing for GUI

## Usage

1. Launch the application.
2. Log in with admin credentials.
3. Navigate through the main menu to manage students, books, and records.

## Database

-- Create the database
CREATE DATABASE LibraryManagementSystem;

-- Use the database
USE LibraryManagementSystem;

-- Create the admin table
CREATE TABLE admin (
id INT AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(50) NOT NULL,
password VARCHAR(255) NOT NULL
);

-- Create the students table
CREATE TABLE students (
student_id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(100) NOT NULL,
department ENUM('AI', 'BC', 'FS') NOT NULL,
year INT NOT NULL
);

-- Create the books table
CREATE TABLE books (
book_id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(100) NOT NULL,
publisher_year YEAR NOT NULL,
price DECIMAL(10, 2) NOT NULL,
author_name VARCHAR(100) NOT NULL
);

-- Create the issued_books table
CREATE TABLE issued_books (
id INT AUTO_INCREMENT PRIMARY KEY,
book_id INT NOT NULL,
student_id INT NOT NULL,
issue_date DATE NOT NULL,
due_date DATE NOT NULL,
FOREIGN KEY (book_id) REFERENCES books(book_id),
FOREIGN KEY (student_id) REFERENCES students(student_id)
);

-- Create the returned_books table
CREATE TABLE returned_books (
id INT AUTO_INCREMENT PRIMARY KEY,
book_id INT NOT NULL,
student_id INT NOT NULL,
return_date DATE NOT NULL,
FOREIGN KEY (book_id) REFERENCES books(book_id),
FOREIGN KEY (student_id) REFERENCES students(student_id)
);

# framework

we have developed a framework using eclipse and jar file was being created by taking out all
the interface and keeping inside the LibraryFramework folder.
imported the LibraryManagement.jar file which is inside the lib directory in all the application
folder which uses the framework.

# Project Structure

LibraryManagementSystem/
├── src/
│ ├── com/
│ │ ├── App.java
│ │ ├── library/
│ │ │ ├── database/
│ │ │ │ ├── Admin.java
│ │ │ │ ├── Book.java  
│ │ │ │ ├── DatabaseManager.java  
│ │ │ │ ├── IssuedBook.java  
│ │ │ │ ├── Student.java  
│ │ │ ├── factory/
│ │ │ │ ├── Form.java
│ │ │ │ ├── FormFactory.java
│ │ │ │ ├── NewStudentForm.java
│ │ │ │ ├── NewBookForm.java
│ │ │ │ ├── IssueBookForm.java
│ │ │ │ └── ReturnBookForm.java
│ │ │ ├── observer/
│ │ │ │ ├── Observer.java
│ │ │ │ ├── Subject.java
│ │ │ │ ├── LibrarySystem.java
│ │ │ ├── search/
│ │ │ │ ├── SearchStrategy.java  
│ │ │ │ ├── SearchByAuthor.java  
│ │ │ │ ├── SearchContext.java  
│ │ │ ├── template/
│ │ │ │ └── LibraryOperation.java
│ │ │ └── ui/
│ │ │ ├── LoginPage.java
│ │ │ ├── MainPage.java  
│ │ │ └── RecordsTable.java  
├── README.md
├── .gitignore
└── lib/
└── mysql-connector-java-8.0.26.jar

<!-- Thank You -->
